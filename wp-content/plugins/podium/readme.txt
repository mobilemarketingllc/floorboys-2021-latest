=== Podium ===

Contributors: Podium
Tags: Podium, Podium Webchat, Webchat, Texting, Chat Widget
Version: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tested up to: 5.0


The official Podium Webchat WordPress plugin, built by Podium. A convenient way to allow cusotmer to text you right from your Website.

== Description ==

[Podium Webchat](https://www.podium.com/webchat/) is the most convenient way to allow customers to text you right from your website.

[youtube https://youtu.be/OlEhw_oB0cg]

== Screenshots ==

1. Podium Webchat widget - minimized
2. Podium Webchat widget - maximized


== Frequently Asked Questions ==

= Installation Instructions =

1. Add this plugin to WordPress and enable it. See [Managing Plugins](https://codex.wordpress.org/Managing_Plugins)
2. Copy your script from your Webchat welcome emails or any other script you are wanting to install.
3. Visit Podium Settings on your WordPress site and paste the script into the textarea.
5. Set the Podium Webchat option to 'Enabled'.
6. Save your Changes.

= Can I use this to embed other scripts on my website =

Yes! This is a free widget that will help you to instantly embed scripts onto your Website!

= How Can I get support =

We are always happy to help you when things are just not working out. Feel free to reach out to us at support@podium.com
